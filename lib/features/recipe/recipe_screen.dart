import 'package:d06_project_flutter/features/recipe/recipe_detail.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class RecipePage extends StatefulWidget {
  const RecipePage({Key? key}) : super(key: key);

  @override
  State<RecipePage> createState() => _RecipePageState();
}

class Recipe {
  final int id;
  final String title;
  final String image;

  Recipe(this.id, this.title, this.image);
}

class _RecipePageState extends State<RecipePage> {
  int _resultFlex = 0;
  List<Recipe> _recipes = [];
  String _loading = "";
  String _keyword = "";
  void _getRecipe(String keyword) async {
    _keyword = keyword;
    final http.Response response = await http.get(Uri.parse(
        'https://d06-project-uas.herokuapp.com/recipe/api/search/' + keyword));

    if (response.statusCode == 200) {
      final dynamic responseJson = jsonDecode(response.body);

      if (responseJson['results'].length == 0) {
        _loading = "No result.";
        setState(() {});
        return;
      }

      for (var item in responseJson['results']) {
        _recipes.add(Recipe(
            item['id'], item['title'].toString(), item['image'].toString()));
      }
      _loading = '';
      setState(() {});
    }
  }

  Future<Map> _getRandomRecipe() async {
    final http.Response response = await http.get(
        Uri.parse('https://d06-project-uas.herokuapp.com/recipe/api/random/'));

    if (response.statusCode == 200) {
      Map responseJson = jsonDecode(response.body) as Map;
      String ingredients = '';
      String instructions = '';

      responseJson = responseJson['recipes'][0];

      for (var item in responseJson['extendedIngredients']) {
        ingredients += item['amount'].toString() +
            ' ' +
            item['unit'].toString() +
            ' ' +
            item['name'].toString() +
            '\n';
      }

      for (var item in responseJson['analyzedInstructions'][0]['steps']) {
        instructions +=
            item['number'].toString() + '. ' + item['step'].toString() + '\n';
      }

      responseJson['ingredients'] = ingredients;
      responseJson['instructions'] = instructions;

      return responseJson;
    }
    return {};
  }

  Future<Map> _getRecipeDetail(int id) async {
    final http.Response response = await http.get(Uri.parse(
        'https://d06-project-uas.herokuapp.com/recipe/api/detail/' +
            id.toString()));

    if (response.statusCode == 200) {
      final Map responseJson = jsonDecode(response.body) as Map;
      String ingredients = '';
      String instructions = '';

      for (var item in responseJson['extendedIngredients']) {
        ingredients += item['amount'].toString() +
            ' ' +
            item['unit'].toString() +
            ' ' +
            item['name'].toString() +
            '\n';
      }

      for (var item in responseJson['analyzedInstructions'][0]['steps']) {
        instructions +=
            item['number'].toString() + '. ' + item['step'].toString() + '\n';
      }

      responseJson['ingredients'] = ingredients;
      responseJson['instructions'] = instructions;

      return responseJson;
    }
    return {};
  }

  Future<List> _getComments(int id) async {
    final http.Response response = await http
        .get(Uri.parse('https://d06-project-uas.herokuapp.com/comment/json'));
    List result = [];

    if (response.statusCode == 200) {
      final dynamic responseJson = jsonDecode(response.body) as List;
      for (var item in responseJson) {
        if (item['fields']['id_number'] == id) {
          result.add(item['fields']);
        }
      }
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final double width = size.width;
    final double height = size.height;
    var _controller = TextEditingController();

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Column(
        children: [
          Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: width / 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _recipes.isEmpty
                      ? const ImageText(
                          text: "My favorite thing to do at home is")
                      : Container(
                          child:
                              ImageText(text: "Results for '" + _keyword + "'"),
                          padding: EdgeInsets.symmetric(
                            horizontal: width / 6,
                          ),
                        ),
                  _recipes.isEmpty
                      ? Container(
                          child: ImageText(text: "COOK          "),
                          padding: EdgeInsets.symmetric(
                            horizontal: width / 9,
                          ),
                        )
                      : Container(),
                  _recipes.isEmpty
                      ? ElevatedButton(
                          onPressed: () async {
                            Map result = await _getRandomRecipe();
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => RecipeDetail(
                                  data: result,
                                  comments: result['id'],
                                ),
                              ),
                            );
                          },
                          child: const Text("randomize!"),
                        )
                      : Container(),
                ],
              ),
              width: width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.25),
                    BlendMode.srcOver,
                  ),
                  fit: BoxFit.cover,
                  image: const AssetImage(
                    "assets/images/food.jpg",
                  ),
                ),
              ),
            ),
          ),
          TextField(
            controller: _controller,
            decoration: InputDecoration(
                prefixIcon: const Icon(Icons.search),
                suffixIcon: IconButton(
                  icon: const Icon(Icons.clear),
                  onPressed: () {
                    _controller.clear();
                    FocusScope.of(context).unfocus();
                    setState(() {
                      _recipes = [];
                      _resultFlex = 0;
                      _loading = "";
                    });
                  },
                ),
                hintText: 'Search recipes...',
                border: InputBorder.none),
            onTap: () {
              setState(() {
                _recipes = [];
                _resultFlex = 0;
                _loading = "";
              });
            },
            onSubmitted: (value) {
              if (value != "") {
                setState(() {
                  _loading = "Loading...";
                });
                setState(() {
                  _resultFlex = 2;
                });
                _getRecipe(value);
              }
            },
          ),
          _recipes.isEmpty
              ? Expanded(
                  flex: _resultFlex,
                  child: Center(
                    child: _loading == "Loading..."
                        ? CircularProgressIndicator()
                        : Container(),
                  ),
                )
              : Expanded(
                  flex: _resultFlex,
                  child: Container(
                    padding: EdgeInsets.only(
                      bottom: height / 20,
                    ),
                    child: ListView.builder(
                      itemCount: _recipes.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () async {
                          Map result =
                              await _getRecipeDetail(_recipes[index].id);
                          List comments =
                              await _getComments(_recipes[index].id);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => RecipeDetail(
                                  data: result, comments: comments),
                            ),
                          );
                        },
                        child: Container(
                          child: Container(
                            alignment: Alignment.bottomLeft,
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              _recipes[index].title,
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          width: width / 1.6,
                          margin: const EdgeInsets.only(
                            left: 20.0,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              colorFilter: ColorFilter.mode(
                                  Colors.black.withOpacity(0.25),
                                  BlendMode.srcOver),
                              image: NetworkImage(_recipes[index].image),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}

class ImageText extends StatelessWidget {
  const ImageText({
    Key? key,
    required this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Text(
        text,
        style: const TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
        textAlign: TextAlign.center,
      ),
      fit: BoxFit.fitWidth,
    );
  }
}
