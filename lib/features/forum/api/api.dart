import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/replies.dart';
import 'package:http/http.dart' as http;

class ReplyProvider with ChangeNotifier {
  ReplyProvider() {
    this.fetchTasks();
  }

  List<Reply> _replies = [];

  List<Reply> get replies {
    return [..._replies];
  }

  Future<dynamic> fetchTasks() async{
    const url = "https://d06-project-uas.herokuapp.com/forum/replies/?format=json";
    final http.Response response = await http.get(Uri.parse(url));

    print(response.statusCode);

    if (response.statusCode == 200){
      var data = json.decode(response.body);

      _replies =data.map<Reply>((json) => Reply.fromJson(json)).toList();

    }
    else{

    }

  }
}