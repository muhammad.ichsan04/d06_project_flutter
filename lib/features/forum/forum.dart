import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import './widgets/main_drawer.dart';
import './widgets/main_container.dart';
import './widgets/bottom_appbar.dart';
import 'package:provider/provider.dart';
import './api/api.dart';
import '../replyforum/createpost_form.dart';

void main() {
  runApp(const ForumPage());
}

class ForumPage extends StatelessWidget {

  const ForumPage({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context)=>ReplyProvider(),
      child:  MaterialApp(
      title: 'D06 | Forum',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
      ),
      home: const MyForumPage(title: 'Forum'),
    ));
  }
}

class MyForumPage extends StatefulWidget {
  const MyForumPage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyForumPage> createState() => _MyForumPageState();
}

class _MyForumPageState extends State<MyForumPage> {
  int _likeCount = 0;
  int _dislikeCount = 0;
  bool disliked = false;
  bool liked = false;

  void _incrementLike() {
    setState (() {
      if (liked != true){
        _likeCount++;
        liked = true;
      }
      if (disliked == true){
        disliked = false;
        _dislikeCount--;
      }
    });
  }

  void _decrementLike() {
    setState(() {
      _likeCount--;
      liked = false;
    });
  }

  void _incrementDislike() {
    setState (() {
      if (disliked != true) {
        _dislikeCount++;
        disliked = true;
      }
      if (liked == true){
        liked = false;
        _likeCount--;
      }
    });
  }

  void _decrementDislike() {
    setState(() {
      _dislikeCount--;
      disliked = false;
    });
  }

  @override
  Widget build(BuildContext context){
    final replyP = Provider.of<ReplyProvider>(context);
    return Scaffold(
      backgroundColor: Color(0xFFCAB7A1),
      body: ListView.builder(
        itemCount:replyP.replies.length,
        itemBuilder:(BuildContext context, int index) {
          return Container(
              color: Color(0xffAC3834),
              alignment: Alignment.center,
              margin: EdgeInsets.all(25.0),
              padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 20.0),
              child: Column(
                  children:<Widget>[
                    Row(
                      children:<Widget>[
                        Container(
                          color:Color(0xFF811112),
                          width:300.0,
                          padding: EdgeInsets.all(5.0),
                          child:Text(DateTime.parse(replyP.replies[index].tanggal).toString().substring(0, 19), style: TextStyle(color:Color(0xFFB29576), fontSize: 11.0, letterSpacing: 3.0)),
                        )
                      ]
                    ),
              Row(
              children:<Widget>[
                  Container(
                  alignment:Alignment.centerLeft,
                  child: Image.network('https://live.staticflickr.com/65535/51634618240_caba66ca65_o.png', width: 60, height:60)
              ),
              Column(children:<Widget>[
                Container(
                    width:160,
                    alignment: Alignment.centerLeft,
                    child: Text(replyP.replies[index].judul,
                      style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:30)),
                    )
                ),
                Container(
                    width:160,
                    alignment: Alignment.centerLeft,
                    child: Text("Posted by ${replyP.replies[index].user}",
                        style: GoogleFonts.sora(textStyle:TextStyle(
                            color: Color(0xFFB29576), letterSpacing:1.3, fontSize:11)))
                ),
              ]),
                PopupMenuButton(
                  onSelected: (result) {
                    if (result == 0) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ReplyForum()),
                      );
                    }
                  },
                  icon: Icon(Icons.more_vert, color: Color(0xFFB29576)),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      child: Text("Reply"),
                      value: 0,
                    ),
                    PopupMenuItem(
                      child: Text("Delete"),
                    ),
                    PopupMenuItem(
                      child: Text("Edit"),
                    ),
                  ],
                )
              ]),
                    Container(
                      color: Colors.white,
                      width:350.0,
                      height:86.0,
                      padding: EdgeInsets.all(5.0),
                      margin: EdgeInsets.all(8.0),
                      child: Text(replyP.replies[index].isi,
                          style: GoogleFonts.sora(textStyle:TextStyle(
                              color: Color(0xFFB29576), letterSpacing: 0.5, fontSize:11))),
                    ),
          Container(
          child:
          Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
          IconButton(
          icon: Icon((liked == false)? Icons.thumb_up_alt_outlined : Icons.thumb_up_alt_sharp, size:20, color:Color(0xFFB29576)),
          onPressed: _incrementLike,
          ),
          Text("${_likeCount}" ,style: TextStyle(color:Color(0xFFB29576))),
          IconButton(
          icon: Icon((disliked == false) ? Icons.thumb_down_alt_outlined : Icons.thumb_down_alt_sharp, size:20, color: Color(0xFFB29576)),
          onPressed: _incrementDislike,
          ),
          Text("${_dislikeCount}", style: TextStyle(color:Color(0xFFB29576)))
          ]
          )
            /*title: Text(replyP.replies[index].judul, style: GoogleFonts.bebasNeue(textStyle: TextStyle(color: Color(0xFF811112), fontSize:30, fontFamily:'BebasNeue')),),
            subtitle: Text(replyP.replies[index].isi, style:GoogleFonts.sora(textStyle:TextStyle(color: Color(0xFFB29576), letterSpacing:1.3, fontSize:11))*/
          )]));
        }
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Color(0xFF811112),
          child: Icon(Icons.add, color: Color(0xFFCAB7A1)), onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ReplyForum()),
        );
    }));}}