import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
        color : Color(0xFFB29576),
        child: ListView(
          children: <Widget>[
            const SizedBox(height:48),
            Container(
              height:200,
              color:Color(0xFFB29576),
              child:
            DrawerHeader(
              child:
              Column(
                children: <Widget>[
                  Image.network('https://live.staticflickr.com/65535/51634618240_caba66ca65_o.png', width:100, height:100,),
                  Text("WELCOME,", style: GoogleFonts.sora(fontSize: 12, letterSpacing: 3, color:Color(0xFFCAB7A1),),),
                  Text("User", style: GoogleFonts.bebasNeue(fontSize:30, color: Color(0xFF811112)))
              ]))),
            buildMenuItem(
              text: 'Home',
              icon: Icons.home,
            ),
            buildMenuItem(
              text: 'Forum',
              icon: Icons.chat,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
  }) {
    final color = Color(0xFFCAB7A1);
    final hoverColor = Color(0xFF881D1D);

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(text, style: TextStyle(color:color)),
      hoverColor: hoverColor,
      onTap : () {},
    );
}
}
