class Reply{
  final String tanggal;
  final String judul;
  final String isi;
  final int user;

  Reply({required this.tanggal, required this.judul, required this.isi, required this.user});

  factory Reply.fromJson(Map<String,dynamic> json){
    return Reply(
      tanggal : json['tanggal'],
      judul : json['judul'],
      isi : json['isi'],
      user : json['user'],
    );
  }
}