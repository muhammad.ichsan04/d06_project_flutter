import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Covid19 extends StatelessWidget {
  final String apiUrl =
      "https://d06-project.herokuapp.com/covid-19/ajax/get_data";
  Future<Map<String, dynamic>> _fecthDataUsers() async {
    var result = await http.get(Uri.parse(apiUrl));
    Map<String, dynamic> map = json.decode(result.body);
    return map;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: FutureBuilder(
          future: _fecthDataUsers(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data['results'].length + 1,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == 0) {
                      return Container(
                        height: 150,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          image: (DecorationImage(
                            image: AssetImage('assets/images/Covid_update.png'),
                            fit: BoxFit.fitWidth,
                          ))
                        ),
                        margin: EdgeInsets.only(bottom:15),
                      );
                    } else {
                      return GestureDetector(
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Container(
                            child: Column(
                              children: <Widget>[
                                Center(
                                  child: Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Text(snapshot.data['results'][index - 1]['name']),
                                  )),
                              ],
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => DetailPage(
                                      snapshot.data['results'][index - 1])));
                        },
                      );
                    }
                  });
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }
}

class DetailPage extends StatelessWidget {
  final Map<String, dynamic> data;
  DetailPage(this.data);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(data['name']),
      ),
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text("Positif : " + data['positive'].toString()),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text("Sembuh : " + data['recovered'].toString()),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text("Meninggal : " + data['deaths'].toString()),
            ),
          ],
        ),
      )
    );
  }
}
// class Covid19 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         body: ListView(
//             children: List.generate(37, (index) {
//           if (index == 0) {
//             return Container(
//               height: 100,
//               width: 200,
//               decoration: BoxDecoration(
//                 image: DecorationImage(
//                   image: AssetImage('assets/images/Covid_update.png'),
//                 ),
//               ),
//             );
//           } 
//           return Card(
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(10.0)),
//             child: Container(
//               child: Column(
//                 children: <Widget>[
//                   Container(
//                     decoration: BoxDecoration(
//                       color: Colors.green,
//                       borderRadius: BorderRadius.circular(10.0),
//                     ),
//                     padding: EdgeInsets.all(10.0),
//                     child: Placeholder(
//                       fallbackHeight: 100.0,
//                     ),
//                   ),
//                   Text('Card ke-$index'),
//                 ],
//               ),
//             ),
//           );
//         })),
//       ),
//     );
//   }
// }
