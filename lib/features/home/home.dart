import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(HomePage());
}
class HomePage extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primarySwatch: Colors.blue),
        home: MyHomePage(title: 'Flutter Demo Home Page'));
  }
}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.
  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        backgroundColor: Color(0xFFCAB7A1),
        body: SingleChildScrollView(
        child: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
            child: Column(
                children: <Widget>[
                  Container(
                      width:500.0,
                      decoration: BoxDecoration(
                        image:DecorationImage(
                          image: NetworkImage('https://i.ibb.co/1LGC0G2/header.png'),
                          fit: BoxFit.cover,
                        )
                      ),
                    child:Container(
                      margin: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
                      child:Container(
                          margin: EdgeInsets.fromLTRB(10.0, 65.0, 0.0, 0.0),
                          child: Text("Welcome", style:GoogleFonts.mrDafoe(fontSize: 70.0, color:Color(0xFF811112), fontWeight: FontWeight.bold,))),
                      width:350.0,
                      height:150.0,

                    )
                  ),
                  Container(
                    color:Color(0xFF811112),
                    width:400.0,
                    child:Text("CREATORS", style:GoogleFonts.sora(fontWeight: FontWeight.bold, fontSize: 12.0, letterSpacing: 4.0, color:Colors.white), textAlign: TextAlign.center),
                    padding: EdgeInsets.all(5.0),
                  ),
                  Container(
                    padding:EdgeInsets.all(5.0),
                    color:Color(0xffAC3834),
                    width:400.0,
                    child:Column(
                      children: <Widget> [
                        Row(
                          children: <Widget> [
                            Container(
                            child:
                              Container(
                                width:100.0,
                                height:100.0,
                                color: Color(0xFF811112),
                                padding: EdgeInsets.all(5.0),
                                child: Image.network('https://d06-project.herokuapp.com/static/img/foto_frans.jpg'),
                                ),
                          ),
                            Container(
                              width:230.0,
                              margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                              child:Column(
                                children : <Widget> [
                                Container(
                              child: Text("Fransisco William Sudianto",
                                style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:25)),
                                )
                            ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                                      child: Text("NPM : 2006596535\nHobby : Belajar, Main, Dengerin Musik, Nonton Film, Wisata Masa Lalu\nQuotes : Tugas bikin pusing, kamu bikin salting",
                                        style: GoogleFonts.sora(textStyle:TextStyle(color: Color(0xFFCAB7A1), fontSize:11, letterSpacing: 0.5)),
                                      )
                                  )
                                ]
                              )
                            )
                        ]),
                        Row(
                            children: <Widget> [
                              Container(
                                  child:
                                  Container(
                                    width:100.0,
                                    height:100.0,
                                    color: Color(0xFF811112),
                                    padding: EdgeInsets.all(5.0),
                                      child: Image.network('https://d06-project.herokuapp.com/static/img/foto_debra.jpeg'),
                                  )
                              ),
                              Container(
                                  width:230.0,
                                  margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                                  child:Column(
                                      children : <Widget> [
                                        Container(
                                            child: Text("Debra Mazaya",
                                              style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:25)), textAlign: TextAlign.left,
                                            )
                                        ),
                                        Container(
                                            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                                            child: Text("NPM : 2006596333\nHobby : FPS Games, Netflix, Graphic Design, Tidur\nQuotes : You gotta do what you have to do",
                                              style: GoogleFonts.sora(textStyle:TextStyle(color: Color(0xFFCAB7A1), fontSize:11, letterSpacing: 0.5)),
                                            )
                                        )
                                      ]
                                  )
                              )
                            ]),
                        Row(
                            children: <Widget> [
                              Container(
                                  child:
                                  Container(
                                    width:100.0,
                                    height:100.0,
                                    color: Color(0xFF811112),
                                    padding: EdgeInsets.all(5.0),
                                      child: Image.network('https://d06-project.herokuapp.com/static/img/foto_ichsan.jpg'),
                                  )
                              ),
                              Container(
                                  width:230.0,
                                  margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                                  child:Column(
                                      children : <Widget> [
                                        Container(
                                            child: Text("Muhammad Ichsan Khairullah",
                                              style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:25)), textAlign: TextAlign.left,
                                            )
                                        ),
                                        Container(
                                            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                                            child: Text("NPM :  2006596264\nHobby : Genshin Impact, Guitar, Running\nQuotes : Aku bukan tidak bisa, ku hanya kurang mencoba",
                                              style: GoogleFonts.sora(textStyle:TextStyle(color: Color(0xFFCAB7A1), fontSize:11, letterSpacing: 0.5)),
                                            )
                                        )
                                      ]
                                  )
                              )
                            ]),
                        Row(
                            children: <Widget> [
                              Container(
                                  child:
                                  Container(
                                    width:100.0,
                                    height:100.0,
                                    color: Color(0xFF811112),
                                    padding: EdgeInsets.all(5.0),
                                      margin: EdgeInsets.fromLTRB(0, 0, 0, 5.0),
                                      child: Image.network('https://i.ibb.co/31Kkznx/S-31473676.jpg'),
                                  )
                              ),
                              Container(
                                  width:230.0,
                                  margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                                  child:Column(
                                      children : <Widget> [
                                        Container(
                                            child: Text("Shadqi Marjan Sadiya",
                                              style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:25)), textAlign: TextAlign.left,
                                            )
                                        ),
                                        Container(
                                            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
                                            child: Text("NPM :  2006596655\nHobby : Makan, Basket\nQuotes : As long as I'm hepi",
                                              style: GoogleFonts.sora(textStyle:TextStyle(color: Color(0xFFCAB7A1), fontSize:11, letterSpacing: 0.5)),
                                            )
                                        )
                                      ]
                                  )
                              )
                            ]),
                        Row(
                            children: <Widget> [
                              Container(
                                  child:
                                  Container(
                                    width:100.0,
                                    height:100.0,
                                    color: Color(0xFF811112),
                                    padding: EdgeInsets.all(5.0),
                                      child: Image.network('https://i.ibb.co/j844GTP/Live-In-191213-0425.jpg'),
                                  )
                              ),
                              Container(
                                  width:230.0,
                                  margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                                  child:Column(
                                      children : <Widget> [
                                        Container(
                                            child: Text("Adietya Christian",
                                              style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:25)), textAlign: TextAlign.left,
                                            )
                                        ),
                                        Container(
                                            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                                            child: Text("NPM :   2006595860\nHobby : Watching Netflix, Playing Rocket League\nQuotes : Don't worry, be happy.",
                                              style: GoogleFonts.sora(textStyle:TextStyle(color: Color(0xFFCAB7A1), fontSize:11, letterSpacing: 0.5)),
                                            )
                                        )
                                      ]
                                  )
                              )
                            ]),
                        Row(
                            children: <Widget> [
                              Container(
                                  child:
                                  Container(
                                    width:100.0,
                                    height:100.0,
                                    color: Color(0xFF811112),
                                    padding: EdgeInsets.all(5.0),
                                      child: Image.network('https://d06-project.herokuapp.com/static/img/foto_elfraera.jpg'),
                                  )
                              ),
                              Container(
                                  width:230.0,
                                  margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                                  child:Column(
                                      children : <Widget> [
                                        Container(
                                            child: Text("Elfraera Winson Barry L. Tobing",
                                              style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:25)), textAlign: TextAlign.left,
                                            )
                                        ),
                                        Container(
                                            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                                            child: Text("NPM : 2006596024\nHobby : Playing COC, Riding\nQuotes : We can do it lah",
                                              style: GoogleFonts.sora(textStyle:TextStyle(color: Color(0xFFCAB7A1), fontSize:11, letterSpacing: 0.5)),
                                            )
                                        )
                                      ]
                                  )
                              )
                            ]),
                        Row(
                            children: <Widget> [
                              Container(
                                  child:
                                  Container(
                                      width:100.0,
                                      height:100.0,
                                      color: Color(0xFF811112),
                                      padding: EdgeInsets.all(5.0),
                                      child: Image.network('https://d06-project.herokuapp.com/static/img/foto_zaki.jpeg'),
                                  )
                              ),
                              Container(
                                  width:230.0,
                                  margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                                  child:Column(
                                      children : <Widget> [
                                        Container(
                                            child: Text("Muhammad Zaki Ash-Shidiqi",
                                              style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:25)), textAlign: TextAlign.left,
                                            )
                                        ),
                                        Container(
                                            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                                            child: Text("NPM : 2006595740\nHobby : Netflix\nQuotes : Bisa yuk, bisa!!!",
                                              style: GoogleFonts.sora(textStyle:TextStyle(color: Color(0xFFCAB7A1), fontSize:11, letterSpacing: 0.5)),
                                            )
                                        )
                                      ]
                                  )
                              )
                            ]),
                      ]
                    ),
                  )
    ]))));
  }
}