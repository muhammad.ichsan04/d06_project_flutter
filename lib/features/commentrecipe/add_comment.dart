import 'dart:convert';
import 'package:http/http.dart' as http;
import 'models/comment.dart';

Future<Comment> createComment(
    int id_number, String name, String rating, String comment) async {
  Comment baru = Comment(
      id_number: id_number, name: name, rating: rating, comment: comment);

  final response = await http.post(
    Uri.parse('https://d06-project-uas.herokuapp.com/api/add-comment/'),
    headers: <String, String>{
      'Content-Type': 'application/json',
    },
    body: commentToJson(baru),
  );
  print(response.statusCode);
  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    Comment comm = Comment.fromJson(jsonDecode(response.body));
    print(comm.id_number);
    return comm;
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to create comment.');
  }
}
