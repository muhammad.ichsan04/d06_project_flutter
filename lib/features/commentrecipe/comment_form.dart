import 'package:flutter/material.dart';
import 'add_comment.dart';
import '../recipe/recipe_detail.dart';
import '../recipe/recipe_screen.dart';

class CommentForm extends StatefulWidget {
  const CommentForm({Key? key, required this.id_number}) : super(key: key);

  final int id_number;

  @override
  State<CommentForm> createState() => CommmentFormState();
}

class CommmentFormState extends State<CommentForm> {
  int id = 0;
  //int id = ;
  String name = '';
  String rating = '';
  String comment = '';

  final _formKey = GlobalKey<FormState>();

  //@override
  //void initState() {
  //  super.initState();
  //  id = widget.id_number;
  //}

  void _trySubmitForm() async {
    final isValid = _formKey.currentState!.validate();

    if (isValid) {
      // print(_confirmPassword);
      //print('Everything looks good!');
      //print(_userEmail);
      print(this.rating);
      print(this.comment);
      print(this.name);
      _formKey.currentState!.save();

      createComment(id, this.name, this.rating, this.comment);
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    id = widget.id_number;
    return Scaffold(
      body: Container(
        color: Color(0xFFB29576),
        alignment: Alignment.center,
        child: Center(
          child: Card(
            margin: EdgeInsets.symmetric(horizontal: 35),
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      /////////////////////////////////////////
                      /// Eamil
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Name', hintText: 'Type your name'),
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            return 'This field is required';
                          }
                          if (value.trim().length < 4) {
                            return 'Username must be at least 4 characters in length';
                          }
                          // Return null if the entered username is valid
                          return null;
                        },
                        onChanged: (value) {
                          this.name = value.toString();
                        },
                      ),

                      /////////////////////////////////////////
                      /// username
                      TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: 'Rating',
                            hintText: 'Type number from 1-5'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'This field is required';
                          }

                          if (int.tryParse(value) == null) {
                            return 'Please enter a number between 1-5';
                          } else {
                            if (int.parse(value) > 5 || int.parse(value) < 1) {
                              return 'Please enter a number between 1-5';
                            }
                          }

                          return null;
                        },
                        onChanged: (value) {
                          this.rating = value.toString();
                        },
                      ),
                      ////////////////////////////////////
                      /// Password
                      TextFormField(
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                            labelText: 'Comment',
                            hintText: 'Type your comment here'),
                        minLines: 1,
                        maxLines: 5,
                        //obscureText: true,
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            return 'This field is required';
                          }
                          // Return null if the entered password is valid
                          return null;
                        },
                        onChanged: (value) {
                          this.comment = value.toString();
                        },
                      ),

                      SizedBox(height: 20),
                      Container(
                          alignment: Alignment.centerRight,
                          child: OutlinedButton(
                              onPressed: _trySubmitForm, child: Text('Submit')))
                    ],
                  )),
            ),
          ),
        ),
      ),
    );
  }
}
