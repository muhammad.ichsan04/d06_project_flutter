import 'dart:convert';

String commentToJson(Comment data) => json.encode(data.toJson());

class Comment {
  int id_number;
  String name;
  String comment;
  String rating;

  Comment({
    required this.id_number,
    required this.name,
    required this.rating,
    required this.comment,
  });

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        id_number: json["id_number"],
        name: json["name"],
        rating: json["rating"],
        comment: json["comment"],
      );

  Map<String, dynamic> toJson() => {
        "id_number": id_number,
        "name": name,
        "rating": rating,
        "comment": comment
      };
}
