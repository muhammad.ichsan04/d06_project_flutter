import 'dart:convert';

Workout workoutFromJson(String str) => Workout.fromJson(json.decode(str));

String workoutToJson(Workout data) => json.encode(data.toJson());

class Workout {
    Workout({
        required this.videos,
    });

    List<Video> videos;

    factory Workout.fromJson(Map<String, dynamic> json) => Workout(
        videos: List<Video>.from(json["videos"].map((x) => Video.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "videos": List<dynamic>.from(videos.map((x) => x.toJson())),
    };
}

class Video {
    Video({
        required this.vidTitle,
        required this.vidAdded,
        required this.vidUrl,
    });

    String vidTitle;
    DateTime vidAdded;
    String vidUrl;

    factory Video.fromJson(Map<String, dynamic> json) => Video(
        vidTitle: json["vidTitle"],
        vidAdded: DateTime.parse(json["vidAdded"]),
        vidUrl: json["vidUrl"],
    );

    Map<String, dynamic> toJson() => {
        "vidTitle": vidTitle,
        "vidAdded": vidAdded.toIso8601String(),
        "vidUrl": vidUrl,
    };
}
