import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:d06_project_flutter/features/workout/models/video.dart';
import 'package:d06_project_flutter/features/workout/widgets/vidcard.dart';
import 'package:d06_project_flutter/features/workout/pages/vidform.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class WorkoutPage extends StatefulWidget {
  final String title;

  WorkoutPage({this.title = 'Workout'});

  @override
  _WorkoutPageState createState() => _WorkoutPageState();
}

class _WorkoutPageState extends State<WorkoutPage> {
  Future <List<Video>> fetchVideo() async {
    var url = Uri.parse('https://d06-project-uas.herokuapp.com/workout/videos/');
    var response =
    await http.get(url, headers: {"Access-Control_Allow_Origin": "*"});
    List<Video> videos = workoutFromJson(response.body).videos;
    return videos;
  }

  // List<Video> vidModelList = [
  //   Video(
  //       vidTitle: "Video 1",
  //       vidAdded: DateTime.now(),
  //       vidUrl: 'https://www.youtube.com/embed/2pLT-olgUJs'),
  //   Video(
  //       vidTitle: "Video 2",
  //       vidAdded: DateTime.now(),
  //       vidUrl: 'https://www.youtube.com/embed/rPPu5RqB_TU'),
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text(widget.title),
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child:
              SizedBox(
                child: Padding(
                  padding: const EdgeInsets.all(0),
                  child: FutureBuilder<List<Video>>(
                    future: fetchVideo(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, index) {
                              return buildVid(snapshot.data![index]);
                            }
                        );
                      } else if (snapshot.hasError) {
                        return Text('${snapshot.error}');
                      }

                      // By default, show a loading spinner.
                      return Center(child: Column(
                        children: const [
                          SizedBox (height: 100),
                          Text("Sedang mengambil data.."),
                          SizedBox (height: 20),
                          SizedBox (width: 30, height: 30, child: CircularProgressIndicator()),
                        ],
                      ),
                      );
                    },
                  ),
                ),
              ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MyCustomForm()),
        ),
        child: const Icon(Icons.add),
        backgroundColor: Colors.red,
      ),
    );
  }
}
