import 'package:flutter/material.dart';
import 'package:d06_project_flutter/features/workout/models/video.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:intl/intl.dart';

Card buildVid(Video video) {
  var heading = video.vidTitle;
  var vidURL = video.vidUrl;
  var vidID = vidURL.substring(30);
  String formattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(video.vidAdded);
    YoutubePlayerController _controller = YoutubePlayerController(
      initialVideoId: vidID,
      flags: YoutubePlayerFlags(
        autoPlay: false,
        mute: false,
      ),
    );
    return Card(
      elevation: 4.0,
      child: Column(
        children: [
          ListTile(
            title: Text(heading),
            subtitle: Text(formattedDate),
          ),
          Container(
            height: 200.0,
            child: YoutubePlayer(
              controller: _controller,
              liveUIColor: Colors.red,
            ),
          ),
        ],
      )
    );
  }