import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:d06_project_flutter/features/workout/models/video.dart';

// Taught by Melati Eka Putri - 2006596655
// https://docs.flutter.dev/cookbook/forms/validation
// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  var _vidTitle = TextEditingController();
  var _vidUrl = TextEditingController();

  void createVid(String vidTitle, String vidUrl) async {

    List<Video> videosList = [];
    Video newVid = Video(vidTitle: vidTitle, vidAdded: DateTime.now(), vidUrl: vidUrl);
    videosList.add(newVid);
    Workout data = Workout(videos:videosList);
    print(workoutToJson(data));

    final response = await http.post(
      Uri.parse('https://d06-project-uas.herokuapp.com/workout/videos/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: workoutToJson(data),
    );

    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.

      _vidTitle.clear();
      _vidUrl.clear();

      FocusScope.of(context).unfocus();
      return;

    } else {
      print(response.body);
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Video tidak ditambahkan');
    }
  }
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Scaffold(
      appBar: AppBar(
        title: Text('Video Form'),
        backgroundColor: Colors.red,
      ),
      body: Container(
        child: Form (
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Title'),
              TextFormField(
            // The validator receives the text that the user has entered.
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Masukkan Judul';
              }
              return null;
            },
                controller: _vidTitle,
          ),
          const Text('Embed URL'),
          TextFormField(
            // The validator receives the text that the user has entered.
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Masukkan URL';
              }
              return null;
            },
            controller: _vidUrl,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false otherwise.
                if (_formKey.currentState!.validate()) {
                  // If the form is valid, display a snackbar. In the real world,
                  // you'd often call a server or save the information in a database.
                  createVid(_vidTitle.text, _vidUrl.text);
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Uploaded!')),
                  );
                }
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      ),
    )
    )
    );
  }
}