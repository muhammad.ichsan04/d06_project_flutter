import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;

class User {
  /*
  This class encapsulates the json response from the api
  {
      'userId': '1908789',
      'username': username,
  }
  */
  String? _userId;
  String? _username;

  constructorUser(String userId, String username) {
    this._userId = userId;
    this._username = username;
  }

  // Properties
  String? get userId => _userId;
  set userId(String? userId) => _userId = userId;

  String? get username => _username;
  set username(String? username) => _username = username;

  // create the user object from json input
  User.fromJson(Map<String, dynamic> json) {
    this._userId = json['userId'].toString();
    this._username = json['username'];
  }

  // exports to json
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this._userId;
    data['username'] = this._username;

    return data;
  }
}

class ApiResponse {
  // _data will hold any response converted into
  // its own object. For example user.
  Object? _data;
  // _apiError will hold the error object
  Object? _apiError;

  Object? get Data => _data;
  set Data(Object? data) => _data = data;

  Object? get ApiError => _apiError as Object;
  set ApiError(Object? error) => _apiError = error;
}

class ApiError {
  String? _error;

  ApiError({String? error}) {
    this._error = error;
  }

  String? get error => _error;
  set error(String? error) => _error = error;

  ApiError.fromJson(Map<String, dynamic> json) {
    _error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this._error;
    return data;
  }
}

// String _baseUrl = "http://10.0.2.2:8000/";
String _baseUrl = "https://d06-project-uas.herokuapp.com/";
Future<ApiResponse> authenticateUser(String username, String password) async {
  ApiResponse _apiResponse = new ApiResponse();

  try {
    final response =
        await http.post(Uri.parse('${_baseUrl}api/user-login/'), body: {
      'username': username,
      'password': password,
    });

    switch (response.statusCode) {
      case 200:
        _apiResponse.Data = User.fromJson(json.decode(response.body));
        developer.log("200");
        break;
      case 401:
        _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
        developer.log("401");
        break;
      default:
        _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
        break;
    }
  } catch (e) {
    if (e is SocketException) {
      _apiResponse.ApiError = ApiError(error: "Server error. Please retry");
    }
  }
  return _apiResponse;
}

Future<ApiResponse> registerUser(String username, String password) async {
  ApiResponse _apiResponse = new ApiResponse();

  try {
    final response =
        await http.post(Uri.parse('${_baseUrl}api/user-create/'), body: {
      'username': username,
      'password': password,
    });

    switch (response.statusCode) {
      case 200:
        _apiResponse.Data = User.fromJson(json.decode(response.body));
        developer.log("200");
        break;
      case 401:
        _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
        developer.log("401");
        break;
      default:
        _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
        break;
    }
  } catch (e) {
    if (e is SocketException) {
      _apiResponse.ApiError = ApiError(error: "Server error. Please retry");
    }
  }
  return _apiResponse;
}

Future<ApiResponse> getUserDetails(String userId) async {
  ApiResponse _apiResponse = new ApiResponse();
  try {
    final response = await http.get(Uri.parse('${_baseUrl}user/$userId'));

    switch (response.statusCode) {
      case 200:
        _apiResponse.Data = User.fromJson(json.decode(response.body));
        break;
      case 401:
        print((_apiResponse.ApiError as ApiError).error);
        _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
        break;
      default:
        print((_apiResponse.ApiError as ApiError).error);
        _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
        break;
    }
  } on SocketException {
    _apiResponse.ApiError = ApiError(error: "Server error. Please retry");
  }
  return _apiResponse;
}
