import 'package:d06_project_flutter/main.dart';
import 'package:http/http.dart' as http;
import 'package:d06_project_flutter/features/accounts/user.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  final swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}

class LoginForm extends StatefulWidget {
  @override
  _LoginForm createState() => _LoginForm();
}

class _LoginForm extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  bool isLoggedIn = false;
  bool isRegister = false;
  late SharedPreferences prefs;
  late ApiResponse _apiResponse;
  late String _username;
  late String _password;
  late String _password2;

  void _handleSubmitted(context, _formKey) async {
    final FormState form = _formKey.currentState;

    if (!form.validate()) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('Please fix the errors in red before submitting.')),
      );
    } else {
      form.save();
      _apiResponse = await authenticateUser(_username, _password);

      if ((_apiResponse.ApiError) == null) {
        _saveAndRedirectToHome();
      } else {
        String response = (_apiResponse.ApiError as ApiError).error.toString();
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(response)));
      }
    }
  }

  void _handleSubmittedRegister(context, _formKey) async {
    final FormState form = _formKey.currentState;

    if (!form.validate()) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('Please fix the errors in red before submitting.')),
      );
    } else if (_password != _password2) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Password confirmation does not match.')),
      );
    } else {
      form.save();
      _apiResponse = await registerUser(_username, _password);

      if ((_apiResponse.ApiError) == null) {
        _saveAndRedirectToLogin();
      } else {
        String response = (_apiResponse.ApiError as ApiError).error.toString();
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(response)));
      }
    }
  }

  void _handleLogout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userId');
    prefs.remove('username');
    prefs.remove('isLoggedIn');
    isLoggedIn = false;

    setState(() {
      isLoggedIn = false;
    });
    // Navigator.of(context).push(
    //   MaterialPageRoute(
    //     builder: (context) => Home()
    //   )
    // );
  }

  void _saveAndRedirectToHome() async {
    prefs = await SharedPreferences.getInstance();

    if ((_apiResponse.Data as User).userId.toString() == "null") {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Username or password is incorrect")));
    } else {
      await prefs.setString(
          "userId", (_apiResponse.Data as User).userId.toString());
      await prefs.setString(
          "username", (_apiResponse.Data as User).username.toString());
      await prefs.setBool("isLoggedIn", true);

      isLoggedIn = true;

      setState(() {
        isLoggedIn = true;
      });
    }
    // Navigator.of(context).push(MaterialPageRoute(
    //     builder: (context) => LogoutForm(
    //           prefs: prefs,
    //         )));
  }

  void _saveAndRedirectToLogin() async {
    prefs = await SharedPreferences.getInstance();

    if ((_apiResponse.Data as User).username.toString() != "null") {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Successfully register account")));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Username is already registered")));
    }

    setState(() {
      isRegister = false;
      isLoggedIn = false;
    });
  }

  Future<String> _getPrefsUsername() async {
    prefs = await SharedPreferences.getInstance();

    return prefs.getString('username').toString();
  }

  Future<bool?> _getPrefsIsLoggedIn() async {
    prefs = await SharedPreferences.getInstance();

    return prefs.getBool('isLoggedIn');
  }

  Future<void> _clearPrefs() async {
    SharedPreferences temp = await SharedPreferences.getInstance();
    await temp.clear();
  }

  Scaffold _loginUI() {
    return Scaffold(
      backgroundColor: Color(0xFFCAB7A1),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      fillColor: Colors.red,
                      labelText: "Username",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    onSaved: (value) {
                      _username = value.toString();
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please insert something';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(
                      fillColor: Colors.red,
                      labelText: "Password",
                      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    onSaved: (value) {
                      _password = value.toString();
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please insert something';
                      }
                      return null;
                    },
                  ),
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Color(0xFFCAB7A1)),
                  ),
                  color: Color(0xFF811112),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      _handleSubmitted(context, _formKey);
                    }
                  },
                ),
                RaisedButton(
                  child: Text(
                    "Register",
                    style: TextStyle(color: Color(0xFFCAB7A1)),
                  ),
                  color: Color(0xFF811112),
                  onPressed: () {
                    setState(() {
                      isLoggedIn = false;
                      isRegister = true;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Scaffold _logoutUI() {
    return Scaffold(
      backgroundColor: Color(0xFFCAB7A1),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child:
                      Text("Welcome back " + prefs.get('username').toString()),
                ),
                RaisedButton(
                    child: Text(
                      "Logout",
                      style: TextStyle(color: Color(0xFFCAB7A1)),
                    ),
                    color: Color(0xFF811112),
                    onPressed: () {
                      _handleLogout();
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }

  Scaffold _registerUI() {
    return Scaffold(
      backgroundColor: Color(0xFFCAB7A1),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      fillColor: Colors.red,
                      labelText: "Username",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    onSaved: (value) {
                      _username = value.toString();
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please insert something';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(
                      fillColor: Colors.red,
                      labelText: "Password",
                      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    onSaved: (value) {
                      _password = value.toString();
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please insert something';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(
                      fillColor: Colors.red,
                      labelText: "Password Confirmation",
                      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    onSaved: (value) {
                      _password2 = value.toString();
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please insert something';
                      }
                      return null;
                    },
                  ),
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Color(0xFFCAB7A1)),
                  ),
                  color: Color(0xFF811112),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      _handleSubmittedRegister(context, _formKey);
                    }
                  },
                ),
                RaisedButton(
                  child: Text(
                    "Login",
                    style: TextStyle(color: Color(0xFFCAB7A1)),
                  ),
                  color: Color(0xFF811112),
                  onPressed: () {
                    setState(() {
                      isLoggedIn = false;
                      isRegister = false;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // _LoginForm() {

  //   _clearPrefs().then((value) => setState(() {
  //         {}
  //       }));

  //   return;
  // }

  _LoginForm() {
    _getPrefsIsLoggedIn().then((value) => setState(() {
          if (value != null && value == true) {
            this.isLoggedIn = value;
          } else {
            {}
          }
        }));
  }

  @override
  Widget build(BuildContext context) {
    if (isLoggedIn == false && isRegister == false) {
      return _loginUI();
    } else if (isLoggedIn == false && isRegister == true) {
      return _registerUI();
    } else {
      return _logoutUI();
    }
  }
}
