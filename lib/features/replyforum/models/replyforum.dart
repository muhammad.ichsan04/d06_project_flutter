//tutorial that helps me :
// https://www.youtube.com/watch?v=VKfiEmhK-no
// https://www.youtube.com/watch?v=v5xGLrhzDGE
import 'dart:convert';

class Replyforum {
  final String title;
  final String content;
  final String date;
  final String user;

  const Replyforum({
    required this.title,
    required this.content,
    required this.date,
    required this.user,
  });

  //Map<String, dynamic> toJson() => _$ReplyforumToJson(this);
}
