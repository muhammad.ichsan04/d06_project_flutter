# D06_Project_Flutter
Kelompok D06
> [Link Heroku](https://d06-project.herokuapp.com/)  
> [Link Gitlab Django](https://gitlab.com/muhammad.ichsan04/d06-project)  
> [Link APK](app-release.apk)  

## Anggota:  
- 2006596535	Fransisco William Sudianto  
- 2006596333	Debra Mazaya  
- 2006596264	Muhammad Ichsan Khairullah  
- 2006596655	Shadqi Marjan Sadiya  
- 2006596024	Elfraera Winson Barry L. Tobing  
- 2006595740	Muhammad Zaki Ash-shidiqi  
- 2006595860	Adietya Christian  

## Penjelasan:  
- Website ini menyediakan berbagai informasi yang dibutuhkan selama masa pandemi seperti video workout yang bisa dilakukan di rumah, resep makanan, update kasus covid, serta forum untuk sharing  

## Integrasi dengan Web Service:
- Server Django akan menyediakan kumpulan data yang dibutuhkan (seperti pada modul resep: resep dan detail resep) berupa JSON yang dapat direquest menggunakan API Endpoint. Data ini kemudian digunakan di dalam aplikasi Flutter.  

## Modul:  
| Modul | Deskripsi |
|---|---|
|accounts| Menyediakan sarana create user, login, dan logout|
|data_covid| Menampilkan status covid (seperti kasus positif, sembuh, dan meninggal) di dunia, Indonesia dan provinsi-provinsinya menggunakan API|
|workout| Menampilkan berbagai video workout menggunakan fitur embed video YouTube yang disediakan oleh API-nya|
|recipe| Menampilkan berbagai resep makanan menggunakan API|
|commentrecipe| Menyediakan sarana untuk post komentar untuk sautu resep makanan tertentu|
|forum| Menyediakan sarana posting forum berupa teks|
|replyforum| Menyediakan sarana untuk reply post forum orang lain|

## Persona:  
- Admin : Dapat melakukan maintenance dan development website  
- User  : Dapat mengakses fitur-fitur website seperti melihat resep makanan, video workout, 
dan forum sharing 
- Guest : Dapat melihat informasi yang ada di website tetapi tidak bisa post atau reply dalam forum   
